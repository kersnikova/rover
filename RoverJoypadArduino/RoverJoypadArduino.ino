// Board:
/// Arduino Nano

#include <LiquidCrystal_I2C.h>
#include <Wire.h>

// Commands (if changing, copy to other sketches!)
#define NOP 0
#define FWD 1
#define BACK 2
#define LEFT 3
#define RIGHT 4
#define STOP 5
#define READ_MOISTURE 6
// ASCII offset when commands are sent as char
#define COMMAND_OFFSET '0'
// if commands are repeated, only send them this often
#define REPEAT_DELAY 300

// Joystick
// TODO: wrap this into library :)
// Sensitivity - 0 is jumpy or skewed in one direction, 512 would never trigger, 200-300 seems ok
#define JOY_SENSITIVITY 300
/// Left
const int joy1XPin = A0, joy1YPin = A1, joy1SwitchPin = 8;
int joy1Command = STOP;
bool joy1SwitchIsDown = false;
/// Right
const int joy2XPin = A2, joy2YPin = A3, joy2SwitchPin = 9;
int joy2Command = STOP;
bool joy2SwitchIsDown = false;

// LCD (Address (Usually 0x27 or 0x3F), Width, Height)
// (connect to SDA, SCL)
// Address, Characters in row, Number of rows 
LiquidCrystal_I2C lcd(0x3F, 16, 2);

void setup() {
  // Init LCD
  lcd.init();
  lcd.backlight();

  // Not woke
  lcd.clear();
  lcd.print("RoveR -_-");
  delay(300);

  // Init joystick
  pinMode(joy1SwitchPin, INPUT_PULLUP);
  pinMode(joy2SwitchPin, INPUT_PULLUP);

  // Init serial
  Serial.begin(57600);

  // Somewhat woke
  lcd.clear();
  lcd.print("RoVeR o_o");
  delay(300);

  // Fully woke
  lcd.clear();
  lcd.print("ROVER O_O");
  delay(300);

  lcd.setCursor(0, 1);
  lcd.print("Waiting for WiFi");
}

void receiveAndDisplayData() { // From NodeMCU
  // Write serial data directly to LCD
  if (Serial.available()) {
    lcd.clear();
    lcd.setCursor(0,0);
    while (Serial.available()) {
      char c = Serial.read();
      if (c == '|') {
        // Start character - clear display
        lcd.clear();
        lcd.setCursor(0,0);
        continue;
      } else  if (c == '\n' || c == '\r') {
        // New line character - go to second line
        lcd.setCursor(0, 1);
        continue;
      }
      lcd.print(c);
    }
  }
}

int lastCommand = NOP;
unsigned long lastCommandTime = 0;
void transmitCommand(int command) { // To NodeMCU
  unsigned long now = millis();
  // No command, or it's not yet time to repeat command
  if (command == NOP) return;
  if (command == lastCommand && now - lastCommandTime < REPEAT_DELAY) return;

  char toSend = COMMAND_OFFSET + command;
  Serial.print(toSend);
  Serial.flush();
  lastCommand = command;
  lastCommandTime = now;
}

// Get direction command from joystick
int getJoyXYCommand(int XPin, int YPin) {
  int Y = analogRead(YPin);
  if (Y < 512 - JOY_SENSITIVITY) {
    return BACK;
  } else if (Y > 512 + JOY_SENSITIVITY) {
    return FWD;
  } else {
    int X = analogRead(XPin);
    if (X < 512 - JOY_SENSITIVITY) {
      return LEFT;
    } else if (X > 512 + JOY_SENSITIVITY) {
      return RIGHT;
    }
  }
  return STOP;
}

// Returns is true only once when switch is pressed, but switchIsDown is set to true if switch is down
int wasJoySWPressed(int SwitchPin, bool &switchIsDown) {
  bool switchState = digitalRead(SwitchPin) == LOW;
  if (switchState == true) {
    if (switchIsDown == false) {
      switchIsDown = true;
      return true;
    }
    switchIsDown = true;
  } else {
    switchIsDown = false;
  }
  return false;
}

void getAndTransmitCommands() { // From Joysticks, To NodeMCU
  bool joy1SwitchWasPressed = wasJoySWPressed(joy1SwitchPin, joy1SwitchIsDown);
  if (joy1SwitchWasPressed) {
    transmitCommand(READ_MOISTURE);
    return;
  }

  /*bool joy1SwitchWasPressed = wasJoySWPressed(joy1SwitchPin, joy1SwitchIsDown);
  bool joy2SwitchWasPressed = wasJoySWPressed(joy2SwitchPin, joy2SwitchIsDown);

  if (joy1SwitchIsDown || joy2SwitchIsDown) {
    if (joy2SwitchWasPressed) {
      transmitCommand(STOP);
    }
    if (joy1SwitchWasPressed) {
      transmitCommand(STOP);
      transmitCommand(READ_MOISTURE);
    }
  } else {
    int joy1XYCommand = getJoyXYCommand(joy1XPin, joy1YPin);
    int joy2XYCommand = getJoyXYCommand(joy2XPin, joy2YPin);
    if (joy1XYCommand != NOP) {
      transmitCommand(joy1XYCommand);
    } else if (joy2XYCommand != NOP) {
      transmitCommand(joy2XYCommand);
    }
  }*/
  int joy1XYCommand = getJoyXYCommand(joy1XPin, joy1YPin);
  int joy2XYCommand = getJoyXYCommand(joy2XPin, joy2YPin);
  if (joy1XYCommand != STOP) {
    transmitCommand(joy1XYCommand);
  } else if (joy2XYCommand != STOP) {
    transmitCommand(joy2XYCommand);
  } else {
    transmitCommand(STOP);
  }
}

void loop() {
  getAndTransmitCommands();
  receiveAndDisplayData();
}



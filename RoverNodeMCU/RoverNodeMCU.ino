// Board:
/// NodeMCU 1.0 (ESP-E12 Module)
// Install: 
/// https://github.com/esp8266/Arduino
/// https://github.com/plerup/espsoftwareserial

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

// Commands (if changing, copy to other sketches!)
#define NOP 0
#define FWD 1
#define BACK 2
#define LEFT 3
#define RIGHT 4
#define STOP 5
#define READ_MOISTURE 6
// ASCII offset when commands are sent as char
#define COMMAND_OFFSET '0'

char strToCommand(String strCommand) {
  if (strCommand == "forward")           return FWD;
  else if (strCommand == "back")         return BACK;
  else if (strCommand == "left")         return LEFT;
  else if (strCommand == "right")        return RIGHT;
  else if (strCommand == "stop")         return STOP;
  else if (strCommand == "readMoisture") return READ_MOISTURE;
  // Default is STOP
  return STOP;
}

// Serial connection to Arduino Mega
//#include <SoftwareSerial.h>
//SoftwareSerial softSerial(D6, D5); // RX, TX
//#define serial softSerial
#define serial Serial
#define BAUDRATE 57600

bool newDataAvailable = false;
String data = "Waiting for data";

// WiFi
IPAddress ip(192,168,1,1);
IPAddress subnet(255,255,255,0);
ESP8266WebServer server(80);
const char
  *ssid = "Rover",
  *password = "RoverPassword";

// Basic user interface
// Connect to Rover and visit http://192.168.1.1/ with browser
void handleRoot() {
  String GUI = "";
  GUI += "<html><head><style>td { text-align: center; font-size: 200%; }</style></head><body>";
  GUI += "<table>";
  GUI += "<tr><td></td><td><a href='/c?command=forward'>NAPREJ</a></td><td></td></tr>";
  GUI += "<tr><td><a href='/c?command=left'>LEVO</a></td><td></td><td><a href='/c?command=right'>DESNO</a></td></tr>";
  GUI += "<tr><td></td><td><a href='/c?command=back'>NAZAJ</a></td><td></td></tr>";
  GUI += "</table>";
  GUI += "</body></html>";
  server.send(200, "text/html", GUI);
}

void handleData() {
  // 200: OK, 304: Not Modified
  int httpCode = newDataAvailable ? 200 : 304;
  server.send(httpCode, "text/plain", data);
}
void handleCommand1() {
  char command = COMMAND_OFFSET + strToCommand(server.arg("command"));
  serial.print(command);
  String response = "OK ";
  response.concat(command);
  server.send(200, "text/plain", response);
}
void handleCommand2() {
  char command = COMMAND_OFFSET + strToCommand(server.arg("command"));
  serial.print(command);
  server.sendHeader("Location", String("/"), true);
  server.send(302, "text/plain", "");
}

void setup() {
  // Turn on LED
  pinMode(D0, OUTPUT);
  pinMode(D4, OUTPUT);
  digitalWrite(D0, LOW);

  serial.begin(BAUDRATE);

  // Create WiFi network
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(ip, ip, subnet);
  boolean isConnected = WiFi.softAP(ssid, password);
  if (!isConnected) {
    delay(1000);
    ESP.reset();
  }

  // Start the server
  server.on("/", handleRoot);
  server.on("/data", handleData);
  server.on("/command", handleCommand1);
  server.on("/c", handleCommand2);
  server.begin();

  digitalWrite(D0, HIGH);
  digitalWrite(D4, LOW);
}

void updateData() {
  if (serial.available()) {
    bool startMarker = false;
    String newData = "";
    while (serial.available()) {
      char c = serial.read();
      if (c == '|') {
        startMarker = true;
        newData = "";
        continue;
      }
      if (c == '$') {
        if (startMarker) {
          // Hooray, full data!
          break;
        }
        if (serial.available()) {
          continue;
        } else {
          break;
        }
      }
      newData += c;
    }
    if (startMarker) {
      data = newData;
    }
    newDataAvailable = true;
  }
}

void loop() {
  server.handleClient();
  updateData();
}

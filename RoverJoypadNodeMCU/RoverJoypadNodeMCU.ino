// Board:
/// NodeMCU 1.0 (ESP-E12 Module)
// Install: 
/// https://github.com/esp8266/Arduino
/// https://github.com/plerup/espsoftwareserial

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

// Commands (if changing, copy to other sketches!)
#define NOP 0
#define FWD 1
#define BACK 2
#define LEFT 3
#define RIGHT 4
#define STOP 5
#define READ_MOISTURE 6
// ASCII offset when commands are sent as char
#define COMMAND_OFFSET '0'

String commandToStr(int command) {
  switch(command) {
    case FWD:   return "forward";
    case BACK:  return "back";
    case LEFT:  return "left";
    case RIGHT: return "right";
    case STOP:  return "stop";
    case READ_MOISTURE: return "readMoisture";
  }
  return "stop";
}

// Serial connection to Arduino Nano
//#include <SoftwareSerial.h>
//SoftwareSerial softSerial(D6, D5); // RX, TX
//#define serial softSerial
#define serial Serial
#define BAUDRATE 57600

// WiFi
const char
  *ssid = "Rover",
  *password = "RoverPassword";

// Received data
String data = "";

void setup() {
  // Turn on LED1
  pinMode(D0, OUTPUT);
  pinMode(D4, OUTPUT);
  digitalWrite(D0, LOW);
  
  serial.begin(BAUDRATE);
  data = "Connecting...";
  transmitData();

  // Connect to WiFi network
  WiFi.begin(ssid, password);
  int i = 0;
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
    i++;
    if (i > 50) {
      data = "Error connecting.";
      transmitData();
      delay(1000);
      ESP.reset();
    }
  }
  data = "Connected";
  transmitData();

  // Turn on LED2, Turn off LED1
  digitalWrite(D4, LOW);
  digitalWrite(D0, HIGH);
}

void transmitCommandsIfAny() { // To Rover
  while (serial.available()) {
    digitalWrite(D0, LOW);
    char commandRaw = serial.read();
    int command = commandRaw - COMMAND_OFFSET;
    String url = "http://192.168.1.1/command?command=" + commandToStr(command);
    HTTPClient http;
    http.begin(url);
    int httpCode = http.GET();
    // Try again one time on error
    if (httpCode != HTTP_CODE_OK) {
      HTTPClient http;
      http.begin(url);
      http.GET();
    }
    digitalWrite(D0, HIGH);
  }
}

void transmitData() { // To Joypad Arduino
  digitalWrite(D4, LOW);
  serial.print("|");
  serial.print(data);
  serial.flush();
  digitalWrite(D4, HIGH);
}
unsigned long lastDataUpdate = 0;
void receiveData() { // From rover
  unsigned long now = millis();
  bool firstDataUpdate = lastDataUpdate == 0;
  if (now - lastDataUpdate > 300 || firstDataUpdate) {
    digitalWrite(D4, LOW);
    lastDataUpdate = now;
    
    HTTPClient httpClient;
    httpClient.begin("http://192.168.1.1/data");
    int httpCode = httpClient.GET();
    if (httpCode == HTTP_CODE_OK) {
      data = httpClient.getString();
      transmitData();
    } else if (httpCode == HTTP_CODE_NOT_MODIFIED) {
      data = httpClient.getString();
      transmitData();
    } else {
      data = "Error receiving data";
    }
    digitalWrite(D4, HIGH);
  }
}

void loop() {
  transmitCommandsIfAny();
  receiveData();
}

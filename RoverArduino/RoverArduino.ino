// Board:
/// Arduino/Genuino Mega or Mega 2560
#ifndef __AVR_ATmega2560__
#error This code was written for Arduino/Genuino Mega or Mega 2560, select it from Tools > Board
#endif

//#include <ST_HW_HC_SR04.h>
#include <AFMotor.h>

// Motors
const int
  bridge1enable1 = 53,
  bridge1in1 = 51,
  bridge1in2 = 49,
  bridge1in3 = 47,
  bridge1in4 = 45,
  bridge1enable2 = 43;

void goForward() {
  digitalWrite(bridge1enable1, HIGH);
  digitalWrite(bridge1enable2, HIGH);
  digitalWrite(bridge1in1, LOW);
  digitalWrite(bridge1in2, HIGH);
  digitalWrite(bridge1in3, LOW);
  digitalWrite(bridge1in4, HIGH);
  digitalWrite(LED_BUILTIN, HIGH);
}
void goBack() {
  digitalWrite(bridge1enable1, HIGH);
  digitalWrite(bridge1enable2, HIGH);
  digitalWrite(bridge1in1, HIGH);
  digitalWrite(bridge1in2, LOW);
  digitalWrite(bridge1in3, HIGH);
  digitalWrite(bridge1in4, LOW);
  digitalWrite(LED_BUILTIN, HIGH);
}
void left() {
  digitalWrite(bridge1enable1, HIGH);
  digitalWrite(bridge1enable2, HIGH);
  digitalWrite(bridge1in1, HIGH);
  digitalWrite(bridge1in2, LOW);
  digitalWrite(bridge1in3, LOW);
  digitalWrite(bridge1in4, HIGH);
  digitalWrite(LED_BUILTIN, HIGH);
}
void right() {
  digitalWrite(bridge1enable1, HIGH);
  digitalWrite(bridge1enable2, HIGH);
  digitalWrite(bridge1in1, LOW);
  digitalWrite(bridge1in2, HIGH);
  digitalWrite(bridge1in3, HIGH);
  digitalWrite(bridge1in4, LOW);
  digitalWrite(LED_BUILTIN, HIGH);
}
void stop() {
  digitalWrite(bridge1enable1, LOW);
  digitalWrite(bridge1enable2, LOW);
  digitalWrite(LED_BUILTIN, LOW);
}

// Commands (if changing, copy to other sketches!)
#define NOP 0
#define FWD 1
#define BACK 2
#define LEFT 3
#define RIGHT 4
#define STOP 5
#define READ_MOISTURE 6
// ASCII offset when commands are sent as char
#define COMMAND_OFFSET '0'

String commandToStr(int command) {
  switch(command) {
    case FWD:   return "forward";
    case BACK:  return "back";
    case LEFT:  return "left";
    case RIGHT: return "right";
    case STOP:  return "stop";
    case READ_MOISTURE: return "readMoisture";
  }
  return "stop";
}
String commandToStrSlo(int command) {
  switch(command) {
    case FWD:   return "naprej";
    case BACK:  return "nazaj";
    case LEFT:  return "levo";
    case RIGHT: return "desno";
    case STOP:  return "stoj";
    case READ_MOISTURE: return "beri vlago";
  }
  return "stoj";
}

int currentCommand = STOP;
unsigned long lastCommandTime = 0;
#define STOP_AFTER_MS 475
#define STOP_AT_CM 25
void enforceCommand(int command) {
  lastCommandTime = millis();
  currentCommand = command;
  switch(command) {
    case FWD:   goForward(); break;
    case BACK:  goBack(); break;
    case LEFT:  left(); break;
    case RIGHT: right(); break;
    case STOP:  stop(); break;
    case READ_MOISTURE:
      readMoisture();
      break;
    default:
      stop();
      break;
  }
  transmitData(true);
}
void stopIfNeeded() {
  //sonarLeftValue  = readSonar(sonarLeft);
  //sonarRightValue = readSonar(sonarRight);
  bool dangerousCommand = currentCommand == FWD || currentCommand == LEFT || currentCommand == RIGHT;
  bool tooClose = dangerousCommand && ((sonarLeftValue > 0 && sonarLeftValue < STOP_AT_CM) || (sonarRightValue > 0 && sonarRightValue < STOP_AT_CM));
  bool autoStop = millis() - lastCommandTime > STOP_AFTER_MS;
  if (tooClose || autoStop) {
    enforceCommand(STOP);
  }
}

// ESP communication
// TODO:
// Not all pins on the Mega and Mega 2560 support change interrupts
// RX: 10, 11, 12, 13, 14, 15, 50, 51, 52, 53,
//     A8 (62), A9 (63), A10 (64), A11 (65), A12 (66), A13 (67), A14 (68), A15 (69). 
//#include <SoftwareSerial.h>
//SoftwareSerial softserial(52, 53); // RX, TX
#define serial Serial1
#define BAUDRATE 57600

// DHT11 temperature/humidity sensor
/// Humidity Range: 20-90% RH
/// Humidity Accuracy: ±5% RH
/// Temperature Range: 0-50 °C
/// Temperature Accuracy: ±2% °C
/// Operating Voltage: 3V to 5.5V
#include <DHT.h>
DHT dht(28, 11); // Pin, DHT Type (11 or 22)

// Moisture sensors
int currentMoisture = 0;
const int moisturePowerPin = 31;
const int moistureDataPin = A8;

// Humidity sensor motor
AF_Stepper stepper(48, 1);

void stick() {
  stepper.setSpeed(255);
  stepper.step(100, FORWARD, SINGLE); 
  stepper.release();
}
void pull() {
  stepper.setSpeed(255);
  stepper.step(100, BACKWARD, SINGLE); 
  stepper.release();
}

unsigned long lastMoistureTime = 0;
void readMoisture() {
  transmitData(true);
  stop();
  if (millis() - lastMoistureTime < 5000) {
    return;
  }
  stick();

  // Turn on sensor wait to stabilise value, always read twice
  digitalWrite(moisturePowerPin, HIGH);
  analogRead(moistureDataPin);
  analogRead(moistureDataPin);
  delay(3000);
  analogRead(moistureDataPin);
  int value = analogRead(moistureDataPin);

  // Turn sensor off
  digitalWrite(moisturePowerPin, LOW);

  currentMoisture = value;
  lastMoistureTime = millis();
  pull();
}

int readTemperature() {
  return dht.readTemperature();
}
int readHumidity() {
  return dht.readHumidity();
}

void setup() {
  serial.begin(BAUDRATE);

  // Init Motors
  pinMode(bridge1enable1, OUTPUT);  
  pinMode(bridge1in1, OUTPUT);  
  pinMode(bridge1in2, OUTPUT);  
  pinMode(bridge1in3, OUTPUT);  
  pinMode(bridge1in4, OUTPUT);  
  pinMode(bridge1enable2, OUTPUT);  
  stop();

  // Init proximity sensors
  // default is 5000us, which is ~75cm
  //int timeout = 30000;
  //sonarLeft.setTimeout(timeout);
  //sonarRight.setTimeout(timeout);

  // Init moisture sensor
  pinMode(moisturePowerPin, OUTPUT);

  // Init DHT sensor
  dht.begin();

  // Init builtin LED
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  digitalWrite(LED_BUILTIN, LOW);  
}

// Distance in cm
int readSonar(ST_HW_HC_SR04 sonar) {
  int distance = sonar.getHitTime() / 29;
  // Retry if no result
  if (distance <= 0) {
    delay(10);
    distance = sonar.getHitTime() / 29;
  }
  if (distance <= 0) {
    delay(10);
    distance = sonar.getHitTime() / 29;
  }

  return distance;
}

char lastReceivedCommand;
unsigned long lastSensorUpdate = 0;
void transmitData() {
  transmitData(false);
}
void transmitData(bool force) {
  unsigned long now = millis();
  if (force || now - lastSensorUpdate > 150) {
    serial.print("|");
    serial.print("Ukaz: ");
    serial.println(commandToStrSlo(currentCommand));

    if (millis() - lastMoistureTime < 5000 && lastMoistureTime != 0) {
      serial.print("Vl. prsti: ");
      //serial.print(currentMoisture);
      //serial.print(" ");
      serial.print((int)((1.0 - currentMoisture/1024.0)*100));
      serial.print("%");
    } else {
      serial.print("T: ");
      serial.print(String(readTemperature()));
      serial.print("C ");
      
      serial.print("V: ");
      serial.print(String(readHumidity()));
      serial.print("% ");
    }
    serial.print("$");

    serial.flush();
    lastSensorUpdate = now;
  }
}

void receiveCommands() {
  while (serial.available()) {
    lastReceivedCommand = serial.read();
    enforceCommand(lastReceivedCommand - COMMAND_OFFSET);
  }
}

void loop() {
  stopIfNeeded();
  receiveCommands();
  stopIfNeeded();
  transmitData();
}

